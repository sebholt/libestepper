/// libestepper: C++ interface description for estepper CNC devices.
/// \copyright See LICENSE-libestepper.txt file

#pragma once

#include <cstdint>

namespace estepper::in
{

/// @brief HOST / PC incoming message types
///
enum Message_Type : std::uint8_t
{
    ERROR,
    UNKNOWN_MESSAGE_TYPE,
    INCOMPLETE_MESSAGE,
    INVALID_CONTROL_INDEX,
    INVALID_STEPPER_INDEX,
    STEPPER_BUFFER_OVERFLOW,
    STEPPER_BAD_SEGMENT,

    PONG,

    SENSOR_1,
    SENSOR_8,
    SENSOR_16,
    SENSOR_32,
    SENSOR_64,

    CONTROL_1,
    CONTROL_8,
    CONTROL_16,
    CONTROL_32,
    CONTROL_64,

    STEPPER_WORDS_PROCESSED,
    STEPPER_DISABLED,

    /// @brief End of message types
    END_ESTEPPER
};

/// @brief Number of message types
constexpr std::size_t const message_type_count =
    static_cast< std::size_t > ( Message_Type::END_ESTEPPER );

/// @brief Segment error types
///
enum Bad_Seg_Type : std::uint8_t
{
    BAD_SEG_UNKNOWN,
    BAD_SEG_BAD_TYPE,
    BAD_SEG_MISSING_DATA_WORD
};

} // namespace estepper::in
