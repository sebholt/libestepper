/// libestepper: C++ interface description for estepper CNC devices.
/// \copyright See LICENSE-libestepper.txt file

#pragma once

#include <cstdint>

namespace estepper::out
{

/// @brief HOST / PC outgoing message types
///
enum Message_Type : std::uint8_t
{
    PING,

    CONTROL_1,
    CONTROL_8,
    CONTROL_16,
    CONTROL_32,
    CONTROL_64,

    STEPPER_ENABLE,
    STEPPER_DISABLE,
    STEPPER_DATA,

    /// @brief End of message types.
    END_ESTEPPER
};

/// @brief Number of message types
constexpr std::size_t const message_type_count =
    static_cast< std::size_t > ( Message_Type::END_ESTEPPER );

} // namespace estepper::out
