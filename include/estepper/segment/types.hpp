/// libestepper: C++ interface description for estepper CNC devices.
/// \copyright See LICENSE-libestepper.txt file

#pragma once

#include <cstdint>

namespace estepper::segment
{

/// @brief Stepper segment types
///
enum Type : std::uint8_t
{
    INVALID,
    // Motor stepper
    MOTOR_SLEEP,
    MOTOR_FORWARD,
    MOTOR_BACKWARD,
    // Controls stepper
    CTL_SLEEP,
    CTL_1,
    CTL_8,
    CTL_16,
    CTL_32,
    CTL_64
};

} // namespace estepper::segment
