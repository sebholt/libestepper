#.rst:
# FindEStepper
# --------
#
# Find EStepper
#
# Find the EStepper includes. This module defines
#
# ::
#
#   ESTEPPER_INCLUDE_DIR, where to find estepper.hpp, etc..
#   EStepper_FOUND, If false, do not try to use EStepper.
#   estepper::estepper imported target.

include ( FindPackageHandleStandardArgs )

# --- Find headers directory
set ( ESTEPPER_SEARCH_HEADER_PATHS
    ${ESTEPPER_SEARCH_HEADER_PATHS}
    "@CMAKE_INSTALL_PREFIX@/@INSTALL_DIR_INCLUDE@" )
find_path ( 
    ESTEPPER_INCLUDE_DIR
    "estepper.hpp"
    PATHS ${ESTEPPER_SEARCH_HEADER_PATHS} )

# handle the QUIETLY and REQUIRED arguments 
# and set ESTEPPER_FOUND to TRUE if
# all listed variables are TRUE
FIND_PACKAGE_HANDLE_STANDARD_ARGS (
    EStepper DEFAULT_MSG ESTEPPER_INCLUDE_DIR )

mark_as_advanced (
    ESTEPPER_SEARCH_HEADER_PATHS )

if ( ESTEPPER_FOUND AND NOT TARGET estepper::estepper )
    add_library ( estepper::estepper INTERFACE IMPORTED )
    set_target_properties ( estepper::estepper PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${ESTEPPER_INCLUDE_DIR}"
    )
endif()
